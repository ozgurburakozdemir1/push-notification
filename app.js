window.addEventListener("load", async () => {
  const subscribeButton = document.querySelector("#subscribeButton");

  // Register Service Worker
  const sW = await navigator.serviceWorker.register("./sw.js");
  console.log("Service Worker => ", sW);

  subscribeButton.addEventListener("click", async () => {
    const serviceWorker = await navigator.serviceWorker.ready;
    const clientID = await serviceWorker.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey:
        "BILx8AEmUL40Ga291-V9Oq37SKfTgnGpZ6Uds7f5kBP7frdvu9Rm8dh2H7p7OdzO_16rgUBgP6tAq9AWsyfy_hQ",
    });
// from test branch
// from test-3 branch
// from 3.0.1-test
// from 3.0.1-test-2
    console.log(clientID);
    console.log(JSON.stringify(clientID));
  });
});
