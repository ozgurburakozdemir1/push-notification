self.addEventListener("push", (e) => {
  console.log("e :>> ", e);
  const config = {
    body: e.data.text() || "Yeni Makaleye Gözatın!!",
    data: {
      dateOfArrival: Date.now(),
      primaryKey: "3",
    },
    icon: "indir.png",
    actions: [
      {
        action: "explore",
        title: "Action1",
        // icon: "images/"
      },
      {
        action: "close",
        title: "Bildirimi Kapat",
        // icon:
      },
    ],
  };
  e.waitUntil(
    self.registration.showNotification("Yeni Makale Eklendi!!", config)
  );
});
